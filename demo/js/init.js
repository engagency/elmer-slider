$(function() {
  // Check for click events on the navbar burger icon
  $(".navbar-burger").click(function(e) {
    e.preventDefault();
    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
    $(".navbar-burger").toggleClass("is-active");
    $(".navbar-menu").toggleClass("is-active");
  });

  var nextCallbackExample = function() {
    new Noty({
      theme: 'relax',
      timeout: 200,
      text: 'next callback'
    }).show();
  };

  var prevCallbackExample = function() {
    new Noty({
      theme: 'relax',
      timeout: 200,
      text: 'prev callback'
    }).show();
  };

  var playCallbackExample = function() {
    new Noty({
      theme: 'relax',
      timeout: 200,
      text: 'play callback'
    }).show();
  };

  var pauseCallbackExample = function() {
    new Noty({
      theme: 'relax',
      timeout: 200,
      text: 'pause callback'
    }).show();
  };

  var hoverInCallbackExample = function() {
    new Noty({
      theme: 'relax',
      timeout: 200,
      text: 'hover in callback'
    }).show();
  };

  var hoverOutCallbackExample = function() {
    new Noty({
      theme: 'relax',
      timeout: 200,
      text: 'hover out callback'
    }).show();
  };

  var initCallbackExample = function() {
    new Noty({
      theme: 'relax',
      timeout: 200,
      text: 'init callback'
    }).show();
  };

  // init elmer sliders
  $('.defaultslider').elmerSlider();

  $('.demoslider').elmerSlider({
    responsive: [
      {
        breakpoint: 0,
        items: 2
      },
      {
        breakpoint: 768,
        items: 4
      },
      {
        breakpoint: 1024,
        items: 6
      }
    ],
    loop: true,
    dots: true,
    autoplay: true,
    autoplaySpeed: 3000,
    swipe: true,
    transitionFunction: 'cubic-bezier(.44,.14,.28,.76)'
  });

  $('.callbackslider').elmerSlider({
    nextCallback: nextCallbackExample,
    prevCallback: prevCallbackExample,
    playCallback: playCallbackExample,
    pauseCallback: pauseCallbackExample,
    hoverInCallback: hoverInCallbackExample,
    hoverOutCallback: hoverOutCallbackExample,
    initCallback: initCallbackExample,
    showPlayControls: true
  });

  $('.heroslider').elmerSlider({
    responsive: [
      {
        items: 1
      }
    ],
    loop: true,
    showPlayControls: true,
    dots: true,
    isHero: true,
    swipe: true
  });

  var progressBarAnimationIndex;

  var progressBarFill = function(el, items, activeItems, isPlaying, autoplaySpeed, transitionTimeRemaining) {
    if (isPlaying) {
      var $bar = $(el).find('.elmer-slider-item--active .progress-bar');

      var progressBarAnimation = function() {
        var barWidth = $bar.width() / $bar.parent().width() * 100;

        if (barWidth >= 100) {
          cancelAnimationFrame(progressBarAnimationIndex);
        }
        else {
          var multiple = autoplaySpeed / 1000;

          var step = 100 / (multiple * 60);
          var newWidth = barWidth + step;
          $bar.width(newWidth + '%');
          progressBarAnimationIndex =requestAnimationFrame(progressBarAnimation);
        }
      };

      progressBarAnimationIndex =requestAnimationFrame(progressBarAnimation);
    }
  };

  var progressBarStop = function(el, items, activeItems, isPlaying, autoplaySpeed, transitionTimeRemaining) {
    cancelAnimationFrame(progressBarAnimationIndex);
  };

  var progressBarReset = function(el, items, activeItems, isPlaying, autoplaySpeed, transitionTimeRemaining) {
    progressBarStop(el, items, activeItems, isPlaying, autoplaySpeed, transitionTimeRemaining);
    $(el).find('.progress-bar').width('0%');
    progressBarFill(el, items, activeItems, isPlaying, autoplaySpeed, transitionTimeRemaining);
  };

  var $fontAwesomeSlider = $('.fontawesomeslider').elmerSlider({
    responsive: [
      {
        items: 1
      }
    ],
    loop: true,
    showPlayControls: true,
    dots: true,
    isHero: true,
    useFontAwesomeIcons: true,
    autoplay: true,
    swipe: true,
    initCallback: progressBarFill,
    prevCallback: progressBarReset,
    nextCallback: progressBarReset,
    playCallback: progressBarFill,
    pauseCallback: progressBarStop,
    hoverInCallback: progressBarStop,
    hoverOutCallback: progressBarFill
  });

  // remove event from items
  $('.item-wrapper').click(function(e) {
    e.preventDefault();
  });

  $('.item-cta').click(function(e) {
    var text = $(this).data('itemValue');
    new Noty({
      theme: 'relax',
      timeout: 200,
      text: text
    }).show();
  })
});