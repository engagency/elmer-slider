(function($) {
  $.elmerSlider = function(el, options) {
    // sanitize data
    if (options) {
      if (options.responsive) {
        // set initial breakpoint
        options.responsive.forEach(function(i) {
          i.breakpoint = i.breakpoint || 0;
        });
        // sort breakpoints
        options.responsive.sort(function(a, b) {
          return (a.breakpoint || 0) - b.breakpoint;
        });
      }
      if (options.autoplay) {
        options.showPlayControls = true;
      }
    }
    // To avoid scope issues, use 'base' instead of 'this'
    // to reference this class from internal events and functions.
    var base = this;
    // Access to jQuery and DOM versions of element
    base.$el = $(el);
    base.el = el;
    // Add a reverse reference to the DOM object
    base.$el.data('elmerSlider', base);
    // variable definitions
    base.currentBreakpointInfo = null;
    base.$dots = null;
    base.$prevControl = null;
    base.$nextControl = null;
    base.$playControl = null;
    base.containerWidth = null;
    base.itemWidth = null;
    base.currentIndex = 1;
    base.numItems = null;
    base.numDots = null;
    base.currentlyWorking = false;
    base.slideIntervalIndexes = [];
    base.latestIntervalIndex = null;
    base.isPlaying = false;
    base.isHoverPaused = false;
    base.translateValue = 0;
    base.itemsToShiftLeft = 0;
    base.transitionTimeStart = null;
    base.transitionTimeEnd = null;
    base.transitionTimeRemaining = null;
    base.useFontAwesomeIcons = false;
    base.static = {
      'base': {
        'default': 'elmer-slider',
        'hero': 'elmer-slider--is-hero'
      },
      'container': {
        'default': 'elmer-slider-container',
        'type': 'div'
      },
      'items': {
        'default': 'elmer-slider-items',
        'notanimated': 'elmer-slider-items--not-animated',
        'type': 'ul'
      },
      'item': {
        'default': 'elmer-slider-item',
        'active': 'elmer-slider-item--active',
        'clone': 'elmer-slider-item--clone',
        'type': 'li'
      },
      'wrapper': 'elmer-slider-container',
      'controls': {
        'default': 'elmer-slider-control',
        'disabled': 'elmer-slider-control--disabled',
        'hidden': 'elmer-slider-control--hidden',
        'faIcons': 'elmer-slider-control--fa-icon',
        'next': {
          'default': 'elmer-slider-control--next',
          'label': 'next slide'
        },
        'prev': {
          'default': 'elmer-slider-control--prev',
          'label': 'previous slide'
        }
      },
      'playControls': {
        'default': 'elmer-slider-play-control',
        'disabled': 'elmer-slider-play-control--disabled',
        'btn': 'elmer-slider-play-control-btn',
        'faIcons': 'elmer-slider-play-control-btn--fa-icon',
        'play': 'elmer-slider-play-control-play',
        'pause': 'elmer-slider-play-control-pause',
        'state': {
          'default': 'elmer-slider-play-control-base',
          'playing': 'elmer-slider-play-control--playing',
          'paused': 'elmer-slider-play-control--paused'
        }
      },
      'dots': {
        'default': 'elmer-slider-dots',
        'label': 'slider pagination'
      },
      'dot': {
        'item': 'elmer-slider-dots-item',
        'default': 'elmer-slider-dot',
        'active': 'elmer-slider-dot--active',
        'label': 'page #'
      }
    };
    base.globalDelay = 10;
    base.addBaseMarkup = function() {
      base.$el.addClass(base.static.base.default).attr('role', 'region').attr('aria-label', 'carousel');
      if (base.options.isHero) {
        base.$el.addClass(base.static.base.hero);
      }
      var $baseContent = base.$el.children();
      base.$el.empty();
      var slideContainer = '<' + base.static.container.type + ' class="' + base.static.container.default+'"></' + base.static.container.type + '>';
      base.$el.append(slideContainer);
      var slideItems = '<' + base.static.items.type + ' class="' + base.static.items.default+'"></' + base.static.items.type + '>';
      base.$el.find('.' + base.static.container.default).append(slideItems);
      if (base.options.transitionFunction) {
        base.$el.find('.' + base.static.items.default).css("transition-timing-function", base.options.transitionFunction);
      }
      $baseContent.each(function() {
        var slideItem = '<' + base.static.item.type + '>' + $(this)[0].outerHTML + '</' + base.static.item.type + '>';
        base.$el.find('.' + base.static.items.default).append(slideItem);
      });
    };
    base.addClones = function() {
      var $prependItems = base.$el.find('.' + base.static.item.default+':not(.' + base.static.item.clone + ')').clone()
        .addClass(base.static.item.clone).removeAttr('tabindex').attr('aria-hidden', false).data("elmerSliderPosition", "front");
      var $appendItems = base.$el.find('.' + base.static.item.default+':not(.' + base.static.item.clone + ')').clone()
        .addClass(base.static.item.clone).removeAttr('tabindex').attr('aria-hidden', false).data("elmerSliderPosition", "back");
      base.$el.find('.' + base.static.items.default).prepend($prependItems);
      base.$el.find('.' + base.static.items.default).append($appendItems);
      base.currentIndex += $prependItems.length;
      base.enableAllSlides();
      base.setSlideClasses(base.$el.find('.' + base.static.item.default).filter(':lt(' + (base.currentIndex - 1) + ')'));
      base.setSlideClasses(base.$el.find('.' + base.static.item.default).filter(':gt(' + (base.currentIndex + base.currentBreakpointInfo.items - 2) + ')'));
      base.setSlideWidth();
      base.setSlidePosition();
    };
    base.setResizeBehavior = function() {
      var currentWidth = window.innerWidth;
      var currentBreakpointInfo;
      var isInitial = false;
      base.options.responsive.forEach(function(el) {
        if (el.breakpoint <= currentWidth) {
          currentBreakpointInfo = el;
        }
      });

      // initial setup
      if (!base.currentBreakpointInfo) {
        isInitial = true;
        base.currentBreakpointInfo = currentBreakpointInfo;
      }
      // only run on breakpoint change
      else if (currentBreakpointInfo.breakpoint != base.currentBreakpointInfo.breakpoint) {
        base.currentBreakpointInfo = currentBreakpointInfo;
        base.updateProperMarkup();
        if (base.options.showPlayControls) {
          base.updateSliderPlayControls();
        }
        if (base.options.dots) {
          base.destroyDots();
          base.addDots();
          base.setActiveDot();
        }
      }
      base.setSlideWidth();
      base.setSlidePosition();
      if (!isInitial) {
        setTimeout(function() {
          base.$el.find('.' + base.static.items.default).removeClass(base.static.items.notanimated);
        }, 1);
      }
    };
    base.addSlideIndexes = function() {
      var $parent = base.$el.find('.' + base.static.items.default).first();
      $parent.children().each(function(index, childEl) {
        $(childEl).attr('data-elmer-slider-index', index + 1);
      });
    };
    base.addProperMarkup = function() {
      var $parent = base.$el.find('.' + base.static.items.default).first();
      $parent.children(':not(.' + base.static.item.clone + ')').each(function(index, childEl) {
        var childElClass = base.static.item.default;
        if (base.options.itemClass) {
          childElClass += ' ' + base.options.itemClass;
        }
        var ariaHiddenLabel = true;
        if (index < base.currentBreakpointInfo.items + base.currentIndex) {
          childElClass += ' ' + base.static.item.active;
          ariaHiddenLabel = false;
        }
        $(childEl).addClass(childElClass).attr('aria-hidden', ariaHiddenLabel);
        if (ariaHiddenLabel) {
          $(childEl).attr('tabindex', -1);
        }
      });
    };
    base.updateProperMarkup = function() {
      var $activeItems = base.$el.find('.' + base.static.item.active);
      if ($activeItems.length > base.currentBreakpointInfo.items) {
        var numberToRemove = $activeItems.length - base.currentBreakpointInfo.items;
        $activeItems.slice((-numberToRemove)).removeClass(base.static.item.active);
      } else {
        var numberToAdd = base.currentBreakpointInfo.items - $activeItems.length;
        var $lastItem = base.$el.find('.' + base.static.item.default).last();
        for (var i = 0; i < numberToAdd; ++i) {
          if (!$lastItem.hasClass(base.static.item.active)) {
            $activeItems.last().next().addClass(base.static.item.active);
          }
          else {
            $activeItems.first().prev().addClass(base.static.item.active);
            if (base.currentIndex > 1) {
              base.currentIndex--;
            }
          }
          $activeItems = base.$el.find('.' + base.static.item.active);
        }
      }
      base.updateSliderControls();
    };
    base.createSliderControls = function(defaultClass, selectorClass, label) {
      return '<button class="' + defaultClass + (base.useFontAwesomeIcons ? ' ' + base.static.controls.faIcons : '') + ' ' + selectorClass + '" aria-label="' + label + '"></button>';
    };
    base.addSliderControls = function() {
      var nextControlMarkup = base.createSliderControls(base.static.controls.default, base.static.controls.next.default, base.static.controls.next.label);
      base.$el.append(nextControlMarkup);
      base.$nextControl = base.$el.find('.' + base.static.controls.next.default).first();
      var prevControlMarkup = base.createSliderControls(base.static.controls.default, base.static.controls.prev.default, base.static.controls.prev.label);
      base.$el.prepend(prevControlMarkup);
      base.$prevControl = base.$el.find('.' + base.static.controls.prev.default).first();
      // add slider control event handlers
      base.$prevControl.click(function(e) {
        if (!$(this).hasClass(base.static.controls.disabled) && !$(this).hasClass(base.static.controls.hidden) && !base.currentlyWorking) {
          base.stopSlider();
          base.clearSliderIntervals();
          base.transitionTimeEnd = 0;
          base.slideSlider({
            direction: 'prev'
          });
        }
      });
      base.$nextControl.click(function(e) {
        if (!$(this).hasClass(base.static.controls.disabled) && !$(this).hasClass(base.static.controls.hidden) && !base.currentlyWorking) {
          base.stopSlider();
          base.clearSliderIntervals();
          base.transitionTimeEnd = 0;
          base.slideSlider({
            direction: 'next'
          });
        }
      });
    };
    base.updateSliderControls = function() {
      if (base.$el.find('.' + base.static.item.default).length <= base.currentBreakpointInfo.items) {
        base.$prevControl.addClass(base.static.controls.hidden);
        base.$nextControl.addClass(base.static.controls.hidden);
      } else {
        base.$prevControl.removeClass(base.static.controls.hidden);
        base.$nextControl.removeClass(base.static.controls.hidden);
        if (!base.options.loop && base.currentIndex == 1) {
          base.$prevControl.addClass(base.static.controls.disabled).attr('aria-disabled', true).attr('tabindex', -1);
        } else {
          base.$prevControl.removeClass(base.static.controls.disabled).removeAttr('aria-disabled').removeAttr('tabindex');
        }
        if (!base.options.loop && base.currentIndex + base.currentBreakpointInfo.items - 1 == base.$el.find('.' + base.static.item.default).length) {
          base.$nextControl.addClass(base.static.controls.disabled).attr('aria-disabled', true).attr('tabindex', -1);
        } else {
          base.$nextControl.removeClass(base.static.controls.disabled).removeAttr('aria-disabled').removeAttr('tabindex');
        }
      }
    };
    base.enableAllSlides = function() {
      base.$el.find('.' + base.static.item.default).addClass(base.static.item.active).removeAttr('tabindex').attr('aria-hidden', false);
    };
    base.slideSlider = function(args) {
      if (base.currentlyWorking || !args || !args.direction) {
        base.setSlidePosition();
        return;
      }
      var slideSliderCallback;
      base.currentlyWorking = true;
      if (args.direction == 'next') {
        if (args.shiftBy) {
          base.currentIndex += args.shiftBy;
        } else {
          base.currentIndex++;
        }
        slideSliderCallback = base.options.nextCallback;
      }
      else if (args.direction == 'prev') {
        if (args.shiftBy) {
          base.currentIndex -= args.shiftBy;
        } else {
          base.currentIndex--;
        }
        slideSliderCallback = base.options.prevCallback;
      }
      base.enableAllSlides();
      base.setSlidePosition(null, slideSliderCallback);
      base.updateSliderControls();
      // reset indexes and kick off a new one in case of interruption
      if (base.options.autoplay && base.isPlaying) {
        var intervalLength = base.slideIntervalIndexes.length;
        while (intervalLength) {
          clearInterval(base.slideIntervalIndexes[intervalLength - 1]);
          base.slideIntervalIndexes.splice(intervalLength - 1, 1);
          --intervalLength;
        }
        base.transitionTimeEnd = 0;
        base.playSlider();
      }
      if (base.options.showPlayControls) {
        base.updateSliderPlayControls();
      }
      if (base.options.dots) {
        base.setActiveDot();
      }
      setTimeout(function() {
        base.resetSlideClasses();

        if (base.options.loop) {
          requestAnimationFrame(function() {
            base.moveAwayFromClones();
          });
        }
      }, base.options.transitionDuration + base.globalDelay);
    };
    base.moveAwayFromClones = function() {
      var $currentActiveItems = base.$el.find('.' + base.static.item.active);
      var activeItemsAreClones = true;
      $currentActiveItems.each(function(i, el) {
        activeItemsAreClones = activeItemsAreClones && $(this).data("elmerSliderPosition") != undefined;
      });
      if (activeItemsAreClones) {
        base.$el.find('.' + base.static.items.default).addClass(base.static.items.notanimated);
        if ($currentActiveItems.data("elmerSliderPosition") == "front") {
          base.$el.find('.' + base.static.items.default).addClass(base.static.items.notanimated);
          base.currentIndex += base.numItems;
        } else if ($currentActiveItems.data("elmerSliderPosition") == "back") {
          base.currentIndex -= base.numItems;
        }
        base.setSlidePosition();
        base.enableAllSlides();
        base.resetSlideClasses();
        setTimeout(function() {
          base.$el.find('.' + base.static.items.default).removeClass(base.static.items.notanimated);
          base.currentlyWorking = false;
        }, base.globalDelay);
      }
    };
    base.resetSlideClasses = function() {
      base.setSlideClasses(base.$el.find('.' + base.static.item.default+':lt(' + (base.currentIndex - 1) + ')'));
      base.setSlideClasses(base.$el.find('.' + base.static.item.default+':gt(' + (base.currentIndex + base.currentBreakpointInfo.items - 2) + ')'));
    };
    base.setSlideClasses = function($elemsToHide) {
      $elemsToHide.removeClass(base.static.item.active).attr('tabindex', -1).attr('aria-hidden', true);
    };
    base.setSlideBehavior = function() {
      base.$el.find('.' + base.static.items.default).on('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function() {
        requestAnimationFrame(function() {
          base.resetSlideClasses();
          base.currentlyWorking = false;
        })
      });
    };
    base.setSlidePosition = function(offset, slideSliderCallback) {
      var offset = offset || 0;
      base.translateValue = -((base.currentIndex - 1) * base.itemWidth);
      base.itemsToShiftLeft = Math.abs(offset) > base.itemWidth ? Math.round(Math.abs(offset) / base.itemWidth) : 0;
      base.itemsToShiftRight = Math.abs(offset) > base.itemWidth ? Math.round(Math.abs(offset) / base.itemWidth) : 0;
      // calculate max number of allowable shifts
      var maxShiftByLeft;
      if (base.options.loop) {
        maxShiftByLeft = base.$el.find('.' + base.static.item.default).length - base.currentIndex - base.currentBreakpointInfo.items + 1;
      }
      else {
        maxShiftByLeft = base.numItems - base.currentBreakpointInfo.items - base.currentIndex + 1;
      }
      if (base.itemsToShiftLeft > maxShiftByLeft) {
        base.itemsToShiftLeft = maxShiftByLeft;
      }
      var maxShiftByRight = base.currentIndex - 1;
      if (base.itemsToShiftRight > maxShiftByRight) {
        base.itemsToShiftRight = maxShiftByRight;
      }
      var currentOffset = 'translateX(' + (base.translateValue + offset) + 'px) translateY(0)';
      base.$el.find('.' + base.static.items.default).css('transform', currentOffset);
      if (slideSliderCallback) {
        slideSliderCallback(base.$el, base.$el.find('.' + base.static.items.default), base.$el.find('.' + base.static.items.active), base.isPlaying, base.options.autoplaySpeed, base.transitionTimeRemaining);
      }
    };
    base.setSlideWidth = function() {
      base.containerWidth = base.$el.find('.' + base.static.wrapper).outerWidth();
      base.itemWidth = base.containerWidth / base.currentBreakpointInfo.items;
      base.$el.find('.' + base.static.item.default).css('width', base.itemWidth + 'px');
      var slideWidth = base.itemWidth * base.$el.find('.' + base.static.item.default).length + 'px';
      base.$el.find('.' + base.static.items.default).css('min-width', slideWidth);
    };
    base.addSliderPlayControls = function() {
      var activeClass = base.options.autoplay ? base.static.playControls.pause : base.static.playControls.play;
      var activeLabel = base.options.autoplay ? 'pause' : 'play';
      var controlMarkup = '<div class="' + base.static.playControls.default+'">';
      controlMarkup +=
        '<button class="' + base.static.playControls.btn + (base.useFontAwesomeIcons ? ' ' + base.static.playControls.faIcons : '') + ' ' + activeClass + '" aria-label="slider ' + activeLabel + ' button">';
      controlMarkup += "</button></div>";
      // add play control dom to slider
      base.$el.prepend(controlMarkup);
      base.$playControl = base.$el.find('.' + base.static.playControls.default).first();
      // add play control event handler
      base.$el.find('.' + base.static.playControls.btn).click(function(e) {
        if (!base.$el.find('.' + base.static.playControls.default).hasClass(base.static.playControls.disabled)) {
          if ($(this).hasClass(base.static.playControls.play)) {
            base.isPlaying = true;
            base.setPlayControlPlay();
            base.playSlider(base.options.playCallback);
          } else {
            base.isPlaying = false;
            base.setPlayControlPause();
            base.clearSliderIntervals();
            base.stopSlider(base.options.pauseCallback);
          }
        }
      });
    };
    base.setPlayControlPlay = function() {
      base.$el.find('.' + base.static.playControls.btn).removeClass(base.static.playControls.play).addClass(base.static.playControls.pause).attr("aria-label", "slider play button");
    };
    base.setPlayControlPause = function() {
      base.$el.find('.' + base.static.playControls.btn).removeClass(base.static.playControls.pause).addClass(base.static.playControls.play).attr("aria-label", "slider pause button");
    };
    base.updateSliderPlayControls = function() {
      if (!base.options.loop && (base.currentIndex - 1 + base.currentBreakpointInfo.items) == base.numItems) {
        base.$playControl.addClass(base.static.playControls.disabled);
        base.stopSlider();
        base.setPlayControlPause();
      } else {
        base.$playControl.removeClass(base.static.playControls.disabled);
      }
    };
    base.playSlider = function(playCallback) {
      base.isPlaying = true;
      if (base.transitionTimeEnd) {
        base.transitionTimeStart = Date.now();
        base.latestIntervalIndex = setTimeout(function() {
          base.slideSlider({
            direction: "next"
          });
          base.transitionTimeEnd = null;
          base.playSlider();
        }, base.transitionTimeRemaining);
        base.slideIntervalIndexes.push(base.latestIntervalIndex);
      } else {
        base.transitionTimeStart = Date.now();
        base.transitionTimeEnd = null;
        base.transitionTimeRemaining = base.options.autoplaySpeed;
        base.latestIntervalIndex = setInterval(function() {

          if (!base.options.loop && base.currentIndex + base.currentBreakpointInfo.items > base.numItems) {
            base.clearSliderIntervals();
            base.stopSlider();
          }
          else {
            base.slideSlider({
              direction: "next"
            });
            base.transitionTimeStart = Date.now();
            base.transitionTimeEnd = null;
          }
        }, base.options.autoplaySpeed);
        base.slideIntervalIndexes.push(base.latestIntervalIndex);
      }
      if (playCallback) {
        playCallback(base.$el, base.$el.find('.' + base.static.items.default), base.$el.find('.' + base.static.items.active), base.isPlaying, base.options.autoplaySpeed, base.transitionTimeRemaining);
      }
    };
    base.stopSlider = function(pauseCallback) {
      base.isPlaying = false;
      base.transitionTimeEnd = Date.now();
      var difference = base.transitionTimeEnd - base.transitionTimeStart;
      base.transitionTimeRemaining = base.transitionTimeRemaining - difference;
      if (pauseCallback) {
        pauseCallback(base.$el, base.$el.find('.' + base.static.items.default), base.$el.find('.' + base.static.items.active), base.isPlaying, base.options.autoplaySpeed, base.transitionTimeRemaining);
      }
    };
    base.addHoverBehavior = function() {
      ua = navigator.userAgent;
      var isIE = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;

      if (window.matchMedia('(hover)').matches || isIE ) {
        base.$el.hover(function() {
          if (base.isPlaying) {
            base.isHoverPaused = true;
            base.clearSliderIntervals();
            base.stopSlider(base.options.hoverInCallback);
          }
        }, function() {
          if (base.isHoverPaused) {
            base.isHoverPaused = false;
            base.clearSliderIntervals();
            if (base.$el.find('.' + base.static.playControls.btn).hasClass(base.static.playControls.pause)) {
              base.playSlider(base.options.hoverOutCallback);
            }
          }
        });
      }
    };
    base.addSwipeBehavior = function() {
      base.$el.find('.' + base.static.items.default).swipe({
        swipeLeft: function(event, distance, duration, fingerCount, fingerData, currentDirection) {
          if (!base.$nextControl.hasClass(base.static.controls.hidden) && !base.$nextControl.hasClass(base.static.controls.disabled)) {
            base.$el.find('.' + base.static.items.default).removeClass(base.static.items.notanimated);
            base.slideSlider({
              direction: 'next',
              'shiftBy': base.itemsToShiftLeft
            });
            setTimeout(function() {
              base.resetSlideClasses();
            }, base.options.transitionDuration);
          } else {
            base.setSlidePosition();
            setTimeout(function() {
              base.resetSlideClasses();
            }, base.options.transitionDuration);
          }
        },
        swipeRight: function(event, distance, duration, fingerCount, fingerData, currentDirection) {
          if (!base.$prevControl.hasClass(base.static.controls.hidden) && !base.$prevControl.hasClass(base.static.controls.disabled)) {
            base.$el.find('.' + base.static.items.default).removeClass(base.static.items.notanimated);
            base.slideSlider({
              direction: 'prev',
              'shiftBy': base.itemsToShiftRight
            });
            setTimeout(function() {
              base.resetSlideClasses();
            }, base.options.transitionDuration);
          } else {
            base.setSlidePosition();
            setTimeout(function() {
              base.resetSlideClasses();
            }, base.options.transitionDuration);
          }
        },
        swipeStatus:function(event, phase, direction, distance) {
          if (!base.options.loop && base.numItems <= base.currentBreakpointInfo.items) {
            return;
          }
          var multiplier;
          if (direction == 'left') {
            multiplier = -1;
          } else if (direction == 'right') {
            multiplier = 1;
          }
          base.setSlidePosition(distance * multiplier);
          base.enableAllSlides();
          base.$el.find('.' + base.static.items.default).addClass(base.static.items.notanimated);
          if (phase == 'cancel' || phase == 'end') {
            base.$el.find('.' + base.static.items.default).removeClass(base.static.items.notanimated);
            if (phase == 'cancel') {
              base.setSlidePosition();
              setTimeout(function() {
                base.resetSlideClasses();
              }, base.options.transitionDuration);
            } else if (phase == 'end') {
              if (direction != 'left' && direction != 'right') {
                base.setSlidePosition();
                setTimeout(function() {
                  base.resetSlideClasses();
                }, base.options.transitionDuration);
              }
            }
          }
        },
        click: function (event, target) {
          // iOS has a problem where click events are not propagated down to the proper
          // element when TouchSwipe is active. On these devices only, we need to manually
          // trigger the click event.
          var iOS = (!!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform)) ||
            (navigator.platform === "MacIntel" && navigator.maxTouchPoints > 0) || navigator.platform === "iPad";

          if (iOS) {
            var $target = $(target);

            if (base.options.swipe) {
              if ($target.is("a")) {
                var href = $target.attr("href");
                var hrefTarget = $target.attr("target");

                if (hrefTarget == "_blank") {
                  window.open(href);
                }
                else {
                  window.location = href;
                }
              }
            }
            else {
              $target.click();
            }
          }
        },
        threshold: 50,
        triggerOnTouchLeave: true,
        allowPageScroll: "vertical"
      });
    };
    base.destroyDots = function() {
      base.$dots.remove();
    };
    base.addDots = function() {
      var isLastPageFull = true;
      if (base.numItems % base.currentBreakpointInfo.items) {
        base.numDots = (Math.floor(base.numItems / base.currentBreakpointInfo.items) + 1);
        isLastPageFull = false;
      } else {
        base.numDots = base.numItems / base.currentBreakpointInfo.items;
      }
      var dotsList = '<ul class="' + base.static.dots.default+'" role="navigation" aria-label="' + base.static.dots.label + '">';
      for (var i = 0; i < base.numDots; ++i) {
        var extraClass = i == 0 ? ' ' + base.static.dot.active : '';
        var isCurrent = i == 0 ? true : false;
        var item =
          '<li class="' + base.static.dot.item + '"><button class="' + base.static.dot.default+extraClass +
          '" aria-label="' + base.static.dot.label + (i + 1) + '" aria-current="' + isCurrent +
          '" data-dot-index="' + (i + 1) + '"></button></li>';
        dotsList += item;
      }
      dotsList += '</ul>';
      base.$el.prepend(dotsList);
      base.$dots = base.$el.find('.' + base.static.dots.default).first();
      // add event handler for each dot
      var $dotItems = base.$dots.find('.' + base.static.dot.item);
      $dotItems.each(function(i, el) {
        $(this).find('.' + base.static.dot.default).click(function(e) {
          if (!$(this).hasClass(base.static.dot.active)) {
            base.$el.find('.' + base.static.dot.default).removeClass(base.static.dot.active);
            var dotIndex = $(this).attr('data-dot-index');
            var indexOffset = base.options.loop ? Math.floor(base.currentIndex / base.numItems) * base.numItems : 0;
            if (!isLastPageFull && dotIndex == base.numDots) {
              base.currentIndex = base.numItems - base.currentBreakpointInfo.items + 1 + indexOffset;
            } else {
              base.currentIndex = base.currentBreakpointInfo.items * (dotIndex - 1) + 1 + indexOffset;
            }
            $(this).addClass(base.static.dot.active);
            // temporarily make all elements visible
            base.enableAllSlides();
            base.setSlidePosition();
            base.updateSliderControls();
            if (base.options.showPlayControls) {
              base.updateSliderPlayControls();
            }

            setTimeout(function() {
              base.resetSlideClasses();

              if (base.options.loop) {
                requestAnimationFrame(function() {
                  base.moveAwayFromClones();
                });
              }
            }, base.options.transitionDuration + base.globalDelay);
          }
        });
      });
    };
    base.setActiveDot = function() {
      var isLastPageFull = base.numItems % base.currentBreakpointInfo.items ? false : true;
      var usableIndex = base.options.loop ? base.currentIndex % base.numItems || base.numItems : base.currentIndex;
      if (!isLastPageFull && usableIndex > base.numItems - base.currentBreakpointInfo.items) {
        dotIndex = base.numDots;
      } else {
        dotIndex = Math.floor((usableIndex - 1) / base.currentBreakpointInfo.items + 1);
      }
      base.$el.find('.' + base.static.dot.default).removeClass(base.static.dot.active);
      base.$el.find('.' + base.static.dot.default+'[data-dot-index="' + dotIndex + '"]').addClass(base.static.dot.active);
    };
    base.temporarilyRemoveAnimation = function() {
      base.$el.find('.' + base.static.items.default).addClass(base.static.items.notanimated);
      requestAnimationFrame(function() {
        base.$el.find('.' + base.static.items.default).removeClass(base.static.items.notanimated);
      });
    };
    base.setKeyboardControls = function() {
      $(document.documentElement).keyup(function(event) {
        if (base.$el.is(":hover")) {
          if (event.keyCode == 37) {
            base.$prevControl.click();
          } else if (event.keyCode == 39) {
            base.$nextControl.click();
          }
        }
      });
    };
    base.checkForFontAwesome = function() {
      var span = document.createElement('span');
      span.className = 'fa';
      span.style.display = 'none';
      document.body.insertBefore(span, document.body.firstChild);
      var fontVal = window.getComputedStyle(span, null).getPropertyValue('font-family');
      var fontList = ['FontAwesome', 'Font Awesome'];
      var isFontPresent = false;
      fontList.forEach(function(el, i) {
        if (fontVal.indexOf(el) !== -1) {
          isFontPresent = true;
        }
      });
      // document.body.removeChild(span);
      return isFontPresent;
    };
    base.clearSliderIntervals = function() {
      var intervalLength = base.slideIntervalIndexes.length;
      while (intervalLength) {
        clearInterval(base.slideIntervalIndexes[intervalLength - 1]);
        base.slideIntervalIndexes.splice(intervalLength - 1, 1);
        --intervalLength;
      }
    };
    base.init = function() {
      base.options = $.extend({}, $.elmerSlider.defaultOptions, options);
      if (base.$el.children().length) {
        base.addBaseMarkup();
        base.setResizeBehavior();
        base.addProperMarkup();
        if (base.options.useFontAwesomeIcons) {
          base.useFontAwesomeIcons = base.checkForFontAwesome();
        }
        base.addSliderControls();
        base.$el.find('.' + base.static.items.default).addClass(base.static.items.notanimated);
        base.setSlideBehavior();
        base.setSlideWidth();
        base.numItems = base.$el.find('.' + base.static.item.default).length;
        if (base.options.keyboardControlsOnHover) {
          base.setKeyboardControls();
        }
        if (base.options.startAt) {
          base.currentIndex = options && options.startAt ? options.startAt : 1;
          base.enableAllSlides();
          base.resetSlideClasses();
          base.setSlidePosition();
        }
        if (base.options.loop) {
          base.setSlideClasses(base.$el.find('.' + base.static.item.default));
          base.addClones();
        } else {
          base.resetSlideClasses();
        }
        if (base.options.showPlayControls) {
          base.addSliderPlayControls();
        }
        if (base.options.dots) {
          base.addDots();
        }
        if (base.options.autoplay) {
          base.isPlaying = true;
          base.playSlider();
        }
        if (base.options.pauseOnHover) {
          base.addHoverBehavior();
        }
        if (base.options.swipe && $.fn.swipe) {
          base.addSwipeBehavior();
        }
        if (base.options.useFontAwesomeIcons) {
          base.useFontAwesomeIcons = base.checkForFontAwesome();
        }
        base.updateSliderControls();
        setTimeout(function() {
          base.$el.find('.' + base.static.items.default).removeClass(base.static.items.notanimated);
        }, 1);
        $(window).resize(function() {
          base.$el.find('.' + base.static.items.default).addClass(base.static.items.notanimated);
          requestAnimationFrame(function() {
            base.setResizeBehavior();
          });
        });
        base.options.initCallback(base.$el, base.$el.find('.' + base.static.items.default), base.$el.find('.' + base.static.items.active), base.isPlaying, base.options.autoplaySpeed, base.transitionTimeRemaining);

        if (base.currentBreakpointInfo.items >= base.numItems) {
          base.setResizeBehavior();
        }
      }
    };
    base.init();
  };
  $.elmerSlider.defaultOptions = {
    responsive: [{
        breakpoint: 0,
        items: 1
      },
      {
        breakpoint: 768,
        items: 3
      },
      {
        breakpoint: 1024,
        items: 5
      }
    ],
    loop: false,
    dots: false,
    autoplay: false,
    transitionDuration: 300,
    showPlayControls: false,
    pauseOnHover: true,
    keyboardControlsOnHover: true,
    autoplaySpeed: 5000,
    swipe: false,
    itemClass: '',
    startAt: 1,
    isHero: false,
    useFontAwesomeIcons: false,
    nextCallback: function(el, items, activeItems, autoplaySpeed, transitionTimeRemaining) {},
    prevCallback: function(el, items, activeItems, autoplaySpeed, transitionTimeRemaining) {},
    playCallback: function(el, items, activeItems, autoplaySpeed, transitionTimeRemaining) {},
    pauseCallback: function(el, items, activeItems, autoplaySpeed, transitionTimeRemaining) {},
    hoverInCallback: function(el, items, activeItems, autoplaySpeed, transitionTimeRemaining) {},
    hoverOutCallback: function(el, items, activeItems, autoplaySpeed, transitionTimeRemaining) {},
    initCallback: function(el, items, activeItems, autoplaySpeed, transitionTimeRemaining) {}
  };
  $.fn.elmerSlider = function(options) {
    return this.each(function() {
      new $.elmerSlider(this, options);
    });
  };
})(jQuery);
