const gulp     = require('gulp');
const concat   = require('gulp-concat');
const uglify   = require('gulp-uglify');
const prefix   = require('gulp-autoprefixer');
const sass     = require('gulp-sass');
const rename   = require('gulp-rename');

// plugin styles
const sassdir = 'src/sass/**/*.scss';
const cssdest = 'dist/css/';
const cssdestfilename = 'elmer-slider.css';

// plugin js
const jsdir  = 'src/js/**/*.js';
const jsdest = 'dist/js/';
const jsdestfilename = 'elmer-slider.js';

// site styles
const sitesassdir = 'demo/sass/**/*.scss';
const sitecssdest = 'demo/min/';
const sitecssdestfilename = 'demo.css';

// site js
const sitejsdir  = 'demo/js/**/*.js';
const sitejsdest = 'demo/min/';
const sitejsdestfilename = 'init.js';

function styles() {
  return gulp.src(sassdir)
    .pipe(sass().on('error', sass.logError))
    .pipe(prefix({browsers: ['defaults', 'ie >= 9']}))
    .pipe(concat(cssdestfilename))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(cssdest))
    .pipe(rename({extname: '.scss'}))
    .pipe(gulp.dest(cssdest));
};

function scripts() {
  return gulp.src(jsdir)
    .pipe(uglify())
    .pipe(concat(jsdestfilename))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(jsdest));
};

function sitestyles() {
  return gulp.src(sitesassdir)
    .pipe(sass().on('error', sass.logError))
    .pipe(prefix({browsers: ['defaults', 'ie >= 9']}))
    .pipe(concat(sitecssdestfilename))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(sitecssdest));  
};

function sitescripts() {
  return gulp.src(sitejsdir)
    .pipe(uglify())
    .pipe(concat(sitejsdestfilename))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(sitejsdest));
};

function watchFiles() {
  gulp.watch(sassdir, styles);
  gulp.watch(jsdir, scripts);

  gulp.watch(sitesassdir, sitestyles);
  gulp.watch(sitejsdir, sitescripts);
};

const build = gulp.parallel(styles, scripts, sitestyles, sitescripts);

const watch = watchFiles;

exports.styles = styles;
exports.scripts = scripts;
exports.build = build;
exports.watch = watch;
exports.default = build;